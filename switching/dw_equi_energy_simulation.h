/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __equi_energy__
#define __equi_energy__

#define _EEFLAG_USEMODEL_  1
#define _EEFLAG_USENEW_    2
#define _EEFLAG_USEOLD_    4

#include "dw_matrix.h"

typedef struct TEquiEnergy_tag
{
  int nlevels;
  TVector loglevels;
  TVector temperatures;
  PRECISION p;                        // probability that we draw from previous level
  int n;                              // number of doubles in data
  int n_theta;                        // number of parameters in theta. n = n_theta + n_q + 1

  int level;
  int ring;
  PRECISION logposteriorkernel;
  PRECISION *draw;

  PRECISION new_logposteriorkernel;
  PRECISION *new_draw;
} TEquiEnergy;

void FreeTEquiEnergy(TEquiEnergy *equi);
TEquiEnergy* CreateTEquiEnergy(TVector loglevels, TVector temperature, PRECISION p, int n, int n_theta, int level);

int GetDraw(TEquiEnergy *equi);
int PutDraw(TEquiEnergy *equi, TStateModel *model, int flag);

int GetRingIndex(TVector loglevels, PRECISION logposteriorkernel, int idx);
int draw_equi_energy(TEquiEnergy *equi, TStateModel *model);

int EquiEnergySimulationRun(int count, TStateMdoel *model, TVector loglevels, TVector temperatures, PRECISION p, int level);
int dw_equi_energy_command_line(int nargs, char **args, TStateModel);

#endif

/********************************************************************************
GetDraw_equi_energy(TStateModel *model, TEquiEnergy *equi, int level)
  

When entering a new level
  1) Initialize generator, which could include adaptive burn-in period.  The more
     usual burn-in could be ommitted for all levels but the first.  The results 
     of this initialization should be available to all instances of this level
  2) 
  


********************************************************************************/
