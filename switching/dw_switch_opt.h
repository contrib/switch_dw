/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MARKOV_SWITCHING_OPTIMIZATION__
#define __MARKOV_SWITCHING_OPTIMIZATION__

#ifdef __cplusplus
extern "C"
{
#endif

#include "dw_switch.h"


void SetupObjectiveFunction(TStateModel *model, PRECISION *Modified, PRECISION *FreeQ, PRECISION *FreeTheta);
void SetupObjectiveFunction_AllocateMemory(TStateModel *model, int FreeTheta_Idx, int FreeQ_Idx, int Modified_Idx);

PRECISION PosteriorObjectiveFunction(PRECISION *x, int n);
PRECISION PosteriorObjectiveFunction_csminwel(double *x, int n, double **args, int *dims);
void PosteriorObjectiveFunction_npsol(int *mode, int *n, double *x, double *f, double *g, int *nstate);

PRECISION MLEObjectiveFunction(PRECISION *x, int n);
PRECISION MLEObjectiveFunction_csminwel(double *x, int n, double **args, int *dims);
void MLEObjectiveFunction_npsol(int *mode, int *n, double *x, double *f, double *g, int *nstate);

int PosteriorOptimization_csminwel(TStateModel *model, int offset, int length, int flag, int max_iteration, 
				   PRECISION tolerance, PRECISION stepsize, PRECISION hessian_scale);

int Estimate_csminwel(TStateModel *model, int iter_block, int iter_start, PRECISION iter_inc, 
			       PRECISION crit_start, PRECISION crit_end, PRECISION crit_inc,
			       PRECISION stepsize_theta, PRECISION hessian_scale_theta,
			       PRECISION stepsize_q, PRECISION hessian_scale_q, 
			       PRECISION stepsize_full, PRECISION hessian_scale_full,FILE *f_out);


#ifdef __cplusplus
}
#endif
#endif
