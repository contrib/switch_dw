/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __METROPOLIS_SIMULATION__
#define __METROPOLIS_SIMULATION__

void dw_Simulate_command_line(int n_args, char **args, TStateModel *model, TVector start_value, int generator_init);
int SetUpMetropolisSimulation(int n_args, char **args, char *tag, TStateModel *model, int default_period);
int Simulate(int burn_in, int draws, int thin, int period, TStateModel *model, FILE *f_out);

#define READ_METROPOLIS_INFO  1
#define HESSIAN_CALIBRATION   2
#define DIAGONAL_CALIBRATION  4
#define TWO_PASS              8
#define SPECIAL_CALIBRATION  16
#define VARIANCE_CALIBRATION 32
#define PRIOR_CALIBRATION    64
int WriteMetropolisThetaInfo(char *filename, char *tag, int type, PRECISION center_s, int period_s, int max_period_s, 
			     PRECISION center_a, int period_a, int max_period_a, int ndraws, int elapsed_time, 
			     TStateModel *model);

#endif

/* Static functions
void SetupDirections(TVector *direction, int n)
*/
