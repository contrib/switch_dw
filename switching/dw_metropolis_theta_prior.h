/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __dw_metropolis_theta_prior__
#define __dw_metropolis_theta_prior__

#include "dw_matrix.h"
#include "dw_switch.h"

#ifdef __cplusplus
extern "C"
{
#endif

  PRECISION draw_metropolis_theta_prior(TStateModel *model, PRECISION logposterior_old);
  void draw_prior_all_metropolis(TStateModel *model);
  //void Draw_metropolis_theta_prior(TStateModel *model);
  //void Draw_metropolis_theta_prior_single_block(int block, TStateModel *model);
  void AdaptiveScale_metropolis_theta_prior(TStateModel *model, PRECISION mid, int period, int max_period, int verbose);
  //void AdaptiveScale_metropolis_theta_prior_single_block(int block, TStateModel *model, PRECISION mid, int period, int max_period, int verbose);
  int Calibrate_metropolis_theta_prior(TStateModel *model, PRECISION center, int period, int max_period, int verbose);
  //int Calibrate_metropolis_theta_prior_single_direction(int idx, TStateModel *model, PRECISION center, int period, int max_period, int verbose);
  //int Calibrate_metropolis_theta_prior_two_pass(TStateModel *model, PRECISION center_s, int period_s, int max_period_s, PRECISION center_a, int period_a, int max_period_a, int verbose);


#ifdef __cplusplus
}
#endif  
  
#endif
