/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __STATE_SPACE_HISTORICAL_DECOMPOSITION__
#define __STATE_SPACE_HISTORICAL_DECOMPOSITION__

#ifdef __cplusplus
extern "C" {
#endif

#include "dw_matrix.h"
#include "dw_switch.h"
#include "dw_MSStateSpace.h"

TMatrix dw_state_space_historical_decomposition(TMatrix var, int t0, int h, TStateModel *model);
TMatrix dw_state_space_historical_decomposition_states(TMatrix var, int t0, int h, TStateModel *model);

TVector* SmoothedShocks(TStateModel *model);
TVector* SmoothedStates(TStateModel *model);

void CheckHistoricalDecomposition(TMatrix var, int verbose, TStateModel *model);
void CheckHistoricalDecompositionStates(TMatrix var, int verbose, TStateModel *model);

#ifdef __cplusplus
}
#endif

#endif
