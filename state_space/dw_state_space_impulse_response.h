/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __STATE_SPACE_IMPULSE_RESPONSE__
#define __STATE_SPACE_IMPULSE_RESPONSE__


#include "dw_matrix.h"
#include "dw_switch.h"

int dw_state_space_impulse_response_states(TMatrix IR, int *S, int h, TStateModel *model);
int dw_state_space_impulse_response_observables(TMatrix IR, int *S, int h, TStateModel *model);

int dw_state_space_impulse_response_states_percentile_regime(FILE *f_out, int s, TVector percentiles, TMatrix posterior_draws, 
						      int thin, int h, TStateModel *model);
int dw_state_space_impulse_response_observables_percentile_regime(FILE *f_out, int s, TVector percentiles, TMatrix posterior_draws, 
						      int thin, int h, TStateModel *model);
int dw_state_space_impulse_response_states_percentile_ergodic(FILE *f_out, TVector percentiles, TMatrix posterior_draws, 
						              int thin, TVector init_prob, int h, TStateModel *model);
int dw_state_space_impulse_response_observables_percentile_ergodic(FILE *f_out, TVector percentiles, TMatrix posterior_draws, 
							           int thin, TVector init_prob, int h, TStateModel *model);

#ifdef __cplusplus
}
#endif

#endif
