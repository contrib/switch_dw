/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __STATE_SPACE_VARIANCE_DECOMPOSITION__
#define __STATE_SPACE_VARIANCE_DECOMPOSITION__

#ifdef __cplusplus
extern "C" {
#endif

#include "dw_matrix.h"
#include "dw_switch.h"
#include "dw_histogram.h"

void NormalizeVarianceDecomposition(TMatrix VD, int nvars);

TMatrix state_space_variance_decomposition_mean(int draws, FILE *posterior_file, int thin, int ergodic, int horizon, TStateModel *model, int flag);
TMatrix state_space_variance_decomposition_mean_regime(FILE *posterior_file, int thin, int s, int horizon, TStateModel *model, int flag);
TMatrixHistogram* state_space_variance_decomposition_percentiles(int draws, FILE *posterior_file, int thin, int ergodic, int horizon, TStateModel *model, int flag);
TMatrixHistogram* state_space_variance_decomposition_percentiles_regime(FILE *posterior_file, int thin, int s, int horizon, TStateModel *model, int flag);

#ifdef __cplusplus
}
#endif  
  
#endif

