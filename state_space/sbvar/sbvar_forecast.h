/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SBVAR_FORECAST__
#define __SBVAR_FORECAST__

#include "dw_switch.h"
#include "dw_switchio.h"
#include "VARbase.h"
#include "VARio.h"
#include "dw_histogram.h"

TMatrix ForecastHistogramToPercentile(TMatrix F, int data, int horizon, int T, PRECISION percentile, TMatrixHistogram *histogram, TStateModel *model, int flag);
TMatrixHistogram* forecast_percentile(int draws, FILE *posterior_file, int thin, int T, int h, TStateModel *model, int flag);
TMatrixHistogram* forecast_percentile_regime(int draws, FILE *posterior_file, int thin, int s, int T, int h, TStateModel *model, int flag);
void dw_sbvar_forecast_command_line(int nargs, char **args);

#endif