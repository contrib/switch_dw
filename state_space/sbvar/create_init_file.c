/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#include "switch.h"
#include "switchio.h"
#include "VARbase.h"
#include "VARio.h"
#include "VARio_matlab.h"
#include "dw_error.h"
#include "dw_ascii.h"
#include "dw_std.h"

#include <stdlib.h>
#include <string.h>


/*
   Creates a standard initialization file from the matlab and specification file.  
*/
int main(int nargs, char **args)
{
  TStateModel *model;
  FILE *f_out;
  char *filename, *fmt="init_%s.dat", *header="Initial: ";
  
  dw_SetTerminalErrors(ALL_ERRORS);
  dw_SetVerboseErrors(ALL_ERRORS);

  if (nargs != 4)
    {
      printf("Syntax:\n  create_init_file <matlab filename> <specs filename> <file tag>\n");
      dw_exit(0);
    }

  model=Combine_matlab_standard(args[1],args[2]);
  ReadConstantParameters(args[1],model);

  sprintf(filename=(char*)dw_malloc(strlen(fmt) + strlen(args[3]) - 1),fmt,args[3]);
  f_out=dw_CreateTextFile(filename);
  Write_VAR_Specification(f_out,(char*)NULL,model);
  WriteBaseTransitionMatrices(f_out,(char*)NULL,header,model);
  Write_VAR_Parameters(f_out,(char*)NULL,header,model);
  fclose(f_out);
  FreeStateModel(model);
  
  return 0;
}
