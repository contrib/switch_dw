/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __VAR_INPUT_OUTPUT__
#define __VAR_INPUT_OUTPUT__

#include "switch.h"
#include "VARbase.h"

void Write_VAR_Specification(FILE *f, char *filename, TStateModel *model);
TStateModel* Read_VAR_Specification(FILE *f, char *filename);

int Read_VAR_Parameters(FILE *f, char *filename, char *id, TStateModel *model);
int Read_VAR_ParametersFlat(FILE *f_in, TStateModel *model);
int Write_VAR_Parameters(FILE *f, char *filename, char *id, TStateModel *model);
int Write_VAR_ParametersFlat(FILE *f, TStateModel *model, char *fmt);
int Write_VAR_ParametersFlat_Headers(FILE *f_out, TStateModel *model);
int Write_VAR_ParametersFlat_A0_Diagonal_One(FILE *f, TStateModel *model, char *fmt);

#if defined(MATLAB_MEX_FILE) || defined(OCTAVE_MEX_FILE)
#if defined(MATLAB_MEX_FILE)
# include "mat.h"
#else
# include "matio.h"
#endif
int Write_VAR_Parameters_Mat(TStateModel *model, char *filename);
#endif

void ReadAllParameters(FILE *f, char *filename, char *id, TStateModel *model);
void WriteAllParameters(FILE *f, char *filename, char *id, TStateModel *model);

int Read_VAR_Parameters_Original(FILE *f, char *filename, char *header, TStateModel *model);

/* Flag values */
#define F_FLAT   0x0001
#define F_FREE   0x0002

int GetPosteriorDraw(FILE *posterior_file, TStateModel *model, int flag);

/**************** Reading init/est_intermediate/est_final files ****************/
typedef struct
{
  char *in_directory;              // -di (default "")
  char *in_tag;                    // -ft (default NULL)
  char *specification_filename;    // -fs (default NULL)
  char *parameters_filename;       // -fp (default NULL)
  char *parameters_header;         // -ph (default NULL)

  char *specification_filename_actual;
  char *parameters_filename_actual;
  char *parameters_header_actual;

  char *out_directory;             // -do  (default "")
  char *out_filename;              // -fo  (default NULL)
  char *out_tag;                   // -fto (default NULL)
  char *out_header;                // -pho (default NULL)

  int MLE;                         // 1 if -MLE and 0 otherwise

  TStateModel *model;              
} TVARCommandLine;

char* CreateFilenameFromTag(char *fmt, char *tag, char *dir);
char* CreatePath(char *path);

TVARCommandLine* Create_VARCommandLine(void);
void Free_VARCommandLine(TVARCommandLine *cmd);
TVARCommandLine* Base_VARCommandLine(int nargs, char **args);
void OutTag_VARCommandLine(TVARCommandLine *cmd);

TVARCommandLine* CreateTStateModel_VARCommandLine(int nargs, char **args, TVARCommandLine *cmd);
/*******************************************************************************/

#endif
