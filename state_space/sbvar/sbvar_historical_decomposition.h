/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SBVAR_HISTORICAL_DECOMPOSITION__
#define __SBVAR_HISTORICAL_DECOMPOSITION__

#include "dw_matrix.h"
#include "dw_switch.h"

TMatrix sbvar_historical_decomposition(int *S, TStateModel *model);
int* sbvar_most_likely_regime(int t0, int horizon, TStateModel *model);
void dw_sbvar_historical_decomposition_command_line(int nargs, char **args);

#endif
