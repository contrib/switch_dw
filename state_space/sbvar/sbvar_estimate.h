/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#include "dw_sbvar_command_line.h"

typedef struct 
{
  char *csminwel_output_filename;
  char *intermediate_output_filename;
  char *final_output_filename;

  PRECISION criterion_start;
  PRECISION criterion_end;
  PRECISION criterion_increment;

  int max_iterations_start;
  PRECISION max_iterations_increment;

  int max_block_iterations;

  int repeat_max;
  PRECISION repeat_tol_obj;
  PRECISION repeat_tol_parms;

  int random;
  int random_small;
  int random_small_ndraws;
  int random_max;
  PRECISION random_tol_obj;
  PRECISION random_tol_parms;

  TVARCommandLine *cmd;
} TEstimateInfo;

TEstimateInfo* SetupEstimateFromVARCommandLine(int nargs, char **args, TVARCommandLine *cmd);
TEstimateInfo* SetupEstimateFromCommandLine(int nargs, char **args);
void FreeEstimateInfo(TEstimateInfo *info);
void FindMode_VAR_csminwel(TEstimateInfo *estimate, FILE *f);
int RepeatEstimation(TEstimateInfo *info, FILE *f_intermediate, FILE *f_final);
