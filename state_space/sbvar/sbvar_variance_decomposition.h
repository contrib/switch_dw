/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SBVAR_VARIANCE_DECOMPOSITION__
#define __SBVAR_VARIANCE_DECOMPOSITION__

#include "dw_switch.h"
#include "dw_switchio.h"
#include "VARbase.h"
#include "VARio.h"
#include "dw_histogram.h"

void variance_decomposition(TMatrix vd, int *S, TMatrix A0_Xi, TMatrix* B);
void NormalizeVarianceDecomposition(TMatrix vd, T_VAR_Parameters *p);
void ReorderVarianceDecomposition(TMatrix vd, T_VAR_Parameters *p);
TMatrix variance_decomposition_mean(int draws, FILE *posterior_file, int thin, int ergodic, int horizon, TStateModel *model, int flag);
TMatrix variance_decomposition_mean_regime(FILE *posterior_file, int thin, int s, int horizon, TStateModel *model, int flag);
TMatrixHistogram *variance_decomposition_percentiles(int draws, FILE *posterior_file, int thin, int ergodic, int horizon, TStateModel *model, int flag);
TMatrixHistogram *variance_decomposition_percentiles_regime(FILE *posterior_file, int thin, int s, int horizon, TStateModel *model, int flag);
void dw_sbvar_variance_decomposition_command_line(int nargs, char **args);

#endif 