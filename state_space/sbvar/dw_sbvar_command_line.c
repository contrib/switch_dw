/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#include "dw_sbvar_command_line.h"
#include "sbvar_historical_decomposition.h"
#include "VARio.h"
#include "switchio.h"
#include "dw_error.h"
#include "dw_parse_cmd.h"
#include "dw_ascii.h"
#include "dw_std.h"
#include "dw_rand.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
   Top level command line parameters - see each function for specific parameters

     -estimate               : produces estimation - posterior mode or maximum likelihood
     -simulate               : produces simulation run
     -probabilities          : produces smoothed or filtered probabilities
     -forecast               : produce forecast
     -ir                     : produce impulse responses
     -variance_decomposition : produce variance decomposition
     -historical             : produce historical decomposition 

   General command line parameters

     -seed : seed value for random number generation (default = 0 which is a "random" seed)

     -ft <tag>
        The input file tag.  Is used to create input filenames if the -fs or
        -fp options are not present.

     -di <directory>
        If this argument exists, then all input files are in specified directory,
        otherwise the current directory is used

     -do <directory>
        If this argument exists, then all output files are in specified directory,
        otherwise the current directory is used

     -fs <filename>
        If this argument exists, then the specification file name is <filename>. 
        If this argument is not present, the rules for determining the 
        specification file are defined in the comments before the function 
        CreateTStateModel_command_line_sbvar(), which is in this file.

     -fp <filename>
        If this argument exists, then the parameter file name is <filename>.
        If this argument is not present, the rules for determining the 
        parameters file are defined in the comments before the function 
        CreateTStateModel_command_line_sbvar(), which is in this file.

     -ph <header>
        If this argument exists, then the header for the parameters is <header>.  
        If this argument is not present, the rules for determining the 
        parameters header are defined in the comments before the function 
        CreateTStateModel_command_line_sbvar(), which is in this file.

     -MLE
        If -ph or is not present, determines whether the input parameter header is
        "MLE: " or "Posterior mode: ".  Also determines if estimation finds the 
        posterior mode or maximum likelihood.

     -fto <tag>
        The output file tag.  Used to create output filenames.  The default value
        is the value associated with the -ft arguement.

     -fo <filename>
        If this argument exists, then the output file name is <filename>.  If 
        this augument is not present

     -pho <header>
        If this argument exists, then the parameter header used for output is 
        <header>.  The default value is -ph <header>.

*/
int main(int nargs, char** args)
{
  // Set seed value
  dw_initialize_generator(dw_ParseInteger_String(nargs,args,"seed",0));

  // estimation
  if (dw_FindArgument_String(nargs,args,"estimate") != -1)
    dw_sbvar_estimate_command_line(nargs,args);

  // simulation
  if (dw_FindArgument_String(nargs,args,"simulate") != -1)
    dw_sbvar_simulate_command_line(nargs,args);

  // probabilities
  if (dw_FindArgument_String(nargs,args,"probabilities") != -1)
    dw_sbvar_probabilities_command_line(nargs,args);

  // mdd - requires posterior draws
  if (dw_FindArgument_String(nargs,args,"mdd") != -1)
    dw_sbvar_mdd_command_line(nargs,args);

  // forecasts
  if (dw_FindArgument_String(nargs,args,"forecast") != -1)
    dw_sbvar_forecast_command_line(nargs,args);

  // impulse responses - requires posterior draws for error bands
  if (dw_FindArgument_String(nargs,args,"ir") != -1)
    dw_sbvar_impulse_response_command_line(nargs,args);

  // variance decomposition
  if (dw_FindArgument_String(nargs,args,"variance_decomposition") != -1)
    dw_sbvar_variance_decomposition_command_line(nargs,args);

  /* /\* historical decomposition *\/ */
  /* if (dw_FindArgument_String(nargs,args,"historical") != -1) */
  /*   dw_sbvar_historical_decomposition_command_line(nargs,args); */

  /* /\* test code *\/ */
  /* if (dw_FindArgument_String(nargs,args,"test") != -1) */
  /*   dw_sbvar_test_command_line(nargs,args); */

  return 0;
}
