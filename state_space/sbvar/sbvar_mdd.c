/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#include "dw_sbvar_command_line.h"
#include "VARio.h"
#include "dw_mdd_switch.h"
#include "dw_error.h"
#include "dw_parse_cmd.h"

void dw_sbvar_mdd_command_line(int nargs, char **args)
{
  TVARCommandLine *cmd;
  T_VAR_Parameters *p;

  // Setup model and initial parameters
  if (!(cmd=CreateTStateModel_VARCommandLine(nargs,args,(TVARCommandLine*)NULL)))
    dw_Error(MEM_ERR);
  else
    if (!cmd->model)
      dw_UserError("Unable to setup SBVAR from command line parameters");
    else
      if (!cmd->parameters_filename_actual)
	dw_UserError("Unable to initialize parameters from command line parameters");
      else
	{
	  // Use WZ normalization
	  p=(T_VAR_Parameters*)(cmd->model->theta);
	  Setup_WZ_Normalization_VAR(p->A0,p);

      /* printf("mdd new\n"); */
      /* dw_marginal_data_density_command_line(nargs,args,cmd->model,dw_ParseInteger_String(nargs,args,"seed",0)); */
      /* rename("mdd_t6_ms_bvar_7_chol_sv3.out","mdd_t6_ms_bvar_7_chol_sv3_new.out"); */
      /* printf("mdd orig\n"); */
      /* dw_marginal_data_density_command_line_1_1(nargs,args,cmd->model,dw_ParseInteger_String(nargs,args,"seed",0)); */
      /* rename("mdd_t6_ms_bvar_7_chol_sv3.out","mdd_t6_ms_bvar_7_chol_sv3_orig.out"); */

          dw_marginal_data_density_command_line(nargs,args,cmd->model,dw_ParseInteger_String(nargs,args,"seed",0));
	}

  Free_VARCommandLine(cmd);
}
