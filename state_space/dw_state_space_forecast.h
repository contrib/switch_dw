/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __STATE_SPACE_FORECAST__
#define __STATE_SPACE_FORECAST__

#ifdef __cplusplus
extern "C" {
#endif

#include "dw_matrix.h"
#include "dw_switch.h"

TMatrix dw_state_space_forecast(TMatrix forecast, int horizon, TVector z_initial, TVector *shocks, int *R, TStateModel *model);
int dw_state_space_forecast_percentile_regime(FILE *f_out, TVector percentiles, int draws, TMatrix posterior_draws, 
					      int thin, int s, int T, int h, TStateModel *model, int nobs);
int dw_state_space_forecast_percentile(FILE *f_out, TVector percentiles, int draws, TMatrix posterior_draws, 
				       int thin, int T, int h, TStateModel *model,int nobs);

void dw_state_space_forecast_command_line(int nargs, char **args, TStateModel *model);

TVector* dw_state_space_mean_conditional_forecast(TVector *F, PRECISION ***y, int h, int t0, TStateModel *model);
TVector* dw_state_space_mean_unconditional_forecast(TVector *F, int h, int t0, TStateModel *model);

TMatrix Forecast_ConditionalShocks_Special(int horizon, TStateModel *model);

  
#ifdef __cplusplus
}
#endif  
#endif
